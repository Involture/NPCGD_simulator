package npcgd_simulator.name_algebra;

import java.util.Set;

import npcgd_simulator.name_algebra.exceptions.NoRenamingException;

public abstract class Name {

  protected abstract Name conjunctionBase(Base b);
  protected abstract boolean equalsConjunction(Conjunction c);
  protected abstract boolean equalsBase(Base b);
  protected abstract boolean intersectsConjunction(Conjunction c);
  protected abstract boolean intersectsBase(Base b);

  public abstract Pair<Name, Name> split();
  public abstract Name conjunction(Name n);
  public abstract Name rename(Renaming r) throws NoRenamingException;
  public abstract boolean equals(Name n);
  public abstract boolean intersects(Name n);
  public abstract Set<Base> getBases();

}
