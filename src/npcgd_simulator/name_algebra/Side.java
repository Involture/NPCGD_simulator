package npcgd_simulator.name_algebra;

public enum Side {
  LEFT, RIGHT;
	
  protected Side getOther() {
    if (this == LEFT)
      return RIGHT;
    return LEFT;
  }
  
  @Override
  public String toString() {
    if (this == LEFT)
      return "l";
    return "r";
  }
}
