package npcgd_simulator.name_algebra;

import java.util.HashMap;
import java.util.Map;

import npcgd_simulator.name_algebra.exceptions.NoRenamingException;
import npcgd_simulator.name_algebra.exceptions.UnauthorizedIntersectionException;


/*
 * Properties :
 * 1) There is no two names intersecting among the values.
 */

public class Renaming {

  private Map<Integer, Name> bindings;

  public Renaming() {
    this.bindings = new HashMap<Integer, Name>();
  }

  // Keeps property 1 by throwing exception.
  public boolean add(Integer i, Name name) 
      throws UnauthorizedIntersectionException {
    if (this.bindings.containsKey(i))
      return false;
    for (Name n : bindings.values())
      if (name.intersects(n))
        throw new UnauthorizedIntersectionException(name, n);
    this.bindings.put(i, name);
    return true;
  }
  
  // Keeps property 1 by throwing exception.
  public boolean replace(Integer i, Name name)
      throws UnauthorizedIntersectionException {
    if (!this.bindings.containsKey(i))
      return false;
    this.bindings.remove(i);
    for (Name n : bindings.values())
      if (name.intersects(n))
        throw new UnauthorizedIntersectionException(name, n);
    this.bindings.put(i, name);
    return true;
  }

  // Modifies result such that result.suffix = name. Returns result modified.
  // Is static, keeps properties.
  private static Name recInfer(Name result, Suffix s, Name name) {
    if (s.isEmpty())
      return name;
    Pair<Name, Name> pair = result.split();
    if (s.getLast().equals(Side.LEFT))
      return recInfer(pair.getFirst(), s.getPrefix(), name).conjunction(
          pair.getSecond());
    if (s.getLast().equals(Side.RIGHT))
      return pair.getFirst().conjunction(
          recInfer(pair.getSecond(), s.getPrefix(), name));
    throw new Error("can't happen");
  }

  private void infer(Base base, Name image) 
      throws UnauthorizedIntersectionException {
    Integer key = Integer.valueOf(base.getAtom());
    Name result = new Base(-1);
    if (this.bindings.containsKey(key))
      result = this.bindings.get(key);
    result = recInfer(result, base.getSuffix().reverse(), image);
    boolean checked = false;
    if (this.bindings.containsKey(key)) {
      checked = this.replace(key, result);
    }
    else {
      checked = this.add(key, result);
    }
    assert(checked);
  }

  // Assumes 'name' must be renamed in 'image' and infers atom renaming.
  // Infered atom renaming may be partial. Unkown parts of image in the
  // renaming are designated by Base(-1).
  public void infer(Name name, Name image) 
    throws UnauthorizedIntersectionException {
    if (name instanceof Base) {
      this.infer((Base) name,  image);
      return;
    }
    if (name instanceof Conjunction) {
      Conjunction conj = (Conjunction) name;
      Pair<Name, Name> splitedImage = image.split();
      infer(conj.getLeft(), splitedImage.getFirst());
      infer(conj.getRight(), splitedImage.getSecond());
      return;
    }
    assert (false);
    
  }

  public Name apply(Integer i) throws NoRenamingException {
    if (!this.bindings.containsKey(i))
      throw new NoRenamingException(i);
    return this.bindings.get(i);
  }
  
  @Override
  public String toString() {
	StringBuilder builder = new StringBuilder();
	for (Map.Entry<Integer, Name> entry : this.bindings.entrySet()) {
	  builder.append(entry.getKey() + " -> " + entry.getValue() + "\n");
	}
	return builder.toString();
  }

}

