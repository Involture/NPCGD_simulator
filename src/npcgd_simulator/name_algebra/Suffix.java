package npcgd_simulator.name_algebra;

public class Suffix {

  // Fields and Constructors

  private Side last;
  private Suffix prefix;
  public static Suffix theEnd = new Suffix();

  private Suffix() {}

  public Suffix(Side s) {
    this.last = s;
    this.prefix = theEnd;
  }

  protected Suffix(Suffix s) {
    if (s.isEmpty()) {
      throw new Error("Suffix.Suffix(Suffix) : must not happen : empty suffix");
    }
    else {
      this.last = s.getLast();
      this.prefix = new Suffix(s.getPrefix());
    }
  }

  // Accessers and Setters

  protected Side getLast() {
    if (this.isEmpty()) {
      throw new Error("Suffix.getLast : must not happen : empty suffix");
    }
    else {
      return this.last;
    }
  }

  protected Suffix getPrefix() {
    if (this.isEmpty()) {
      throw new Error("Suffix.getPrefix : must not happen : empty suffix");
    }
    else {
      return this.prefix;
    }
  }

  // Class Methods

  public boolean isEmpty() {
    return this == theEnd;
  }

  public Suffix append(Side s) {
    Suffix result = new Suffix();
    result.last = s;
    result.prefix = this;
    return result;
  }

  public Suffix concat(Suffix s) {
    Suffix res = new Suffix(s);
    Suffix revs = this.reverse();
    while (!revs.isEmpty()) {
      res = res.append(revs.getLast());
      revs = revs.getPrefix();
    }
    return res;
  }

  public Suffix pour(Suffix s) {
    Suffix res = new Suffix(s);
    Suffix base = this;
    while (!base.isEmpty()) {
      res = res.append(base.getLast());
      base = base.getPrefix();
    }
    return res;
  }

  public Suffix reverse() {
    Suffix result = Suffix.theEnd;
    Suffix current = this;
    while (!current.isEmpty()) {
      result = result.append(current.getLast());
      current = current.getPrefix();
    }
    return result;
  }

  public boolean prefixes(Suffix s) {
    Suffix argument = s.reverse();
    Suffix prefix = this.reverse();
    while (!prefix.isEmpty() && !argument.isEmpty()) {
      if (prefix.getLast() != argument.getLast()) return false;
      prefix = prefix.getPrefix();
      argument = argument.getPrefix();
    }
    return prefix.isEmpty();
  }

  public boolean phyPrefixes(Suffix s) {
    Suffix argument = s;
    Suffix prefix = this;
    while (!argument.isEmpty()) {
      if (argument == prefix) return true;
      argument = argument.getPrefix();
    }
    return prefix.isEmpty();
  }

  public boolean phyIntersects(Suffix s) {
    return this.phyPrefixes(s) || s.phyPrefixes(this);
  }

  public boolean intersects(Suffix s) {
    return this.prefixes(s) || s.prefixes(this);
  }

  public Name apply(Name name) {
    Suffix reversed = this.reverse();
    Name result = name;
    while (reversed != theEnd) {
      Pair<Name, Name> splitted = result.split();
      if (reversed.getLast().equals(Side.LEFT))
        result = splitted.getFirst();
      if (reversed.getLast().equals(Side.RIGHT))
        result = splitted.getSecond();
      reversed = reversed.getPrefix();
    }
    return result;
  }

  // Inherited Methods (Override or Implementation of abstract)
  
  @Override
  public String toString() {
    if (this.isEmpty())
      return "";
    return this.prefix.toString() + this.last.toString();
  }

  public boolean equals(Suffix s) {
    Suffix suffix1 = this;
    Suffix suffix2 = s;
    while (!suffix1.isEmpty() && !suffix2.isEmpty()) {
      if (suffix1.getLast() != suffix2.getLast()) {
        return false;
      }
      else {
        suffix1 = suffix1.getPrefix();
        suffix2 = suffix2.getPrefix();
      }
    }
    return (suffix1.isEmpty() && suffix2.isEmpty());
  }
  
  @Override
  public int hashCode() {
    int result = 0;
    int mult = 1;
    Suffix suffix = this;
    while (!suffix.isEmpty()) {
      result += ((suffix.getLast() == Side.LEFT) ? 0 : 1) * mult;
      mult *= 2;
      suffix = suffix.getPrefix();
    }
    return result;
  }
  
}
