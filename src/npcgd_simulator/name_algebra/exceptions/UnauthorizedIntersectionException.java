package npcgd_simulator.name_algebra.exceptions;

import npcgd_simulator.name_algebra.Name;

@SuppressWarnings("serial")
public class UnauthorizedIntersectionException extends Exception {

  public UnauthorizedIntersectionException(Name name1, Name name2) {
    super(String.format("[%s] intersects [%s] but should not.", name1, name2));
  }

}
