package npcgd_simulator.name_algebra.exceptions;

@SuppressWarnings("serial")
public class NoRenamingException extends Exception {

  public NoRenamingException(Integer i) {
    super(String.format("No Renaming was found for atom %d", i));
  }

}
