package npcgd_simulator.name_algebra;

import java.util.HashSet;
import java.util.Set;

import npcgd_simulator.name_algebra.exceptions.NoRenamingException;

public class Base extends Name {

  // Fields and Constructors

  private int atom;
  private Suffix suffix;

  protected Base() {};

  public Base(int i) {
    this.atom = i;
    this.suffix = Suffix.theEnd;
  }

  public Base(int i, Suffix s) {
    this.atom = i;
    this.suffix = s;
  }

  // Getters

  public int getAtom() {
    return this.atom;
  }

  public Suffix getSuffix() {
    return this.suffix;
  }
  
  public Base getParent() {
    if (this.suffix.isEmpty())
      return this;
    return new Base(this.atom, this.suffix.getPrefix());
  }
  
  public Base getBrother() {
    if(this.suffix.isEmpty())
      return this;
    return new Base(this.atom, 
        this.suffix.getPrefix().append(this.suffix.getLast().getOther()));
  }
  
  // Class Methods
  
  private static void fuse(Base b, Set<Base> bases) {
    if (b.getSuffix().isEmpty())
      return;
    if (bases.contains(b.getBrother())) {
      bases.remove(b.getBrother());
      fuse(b.getParent(), bases);
    }
    bases.add(b);
  }
	  
  public static Set<Base> LonelyBases(Set<Base> bases) {
    Set<Base> lonelyBases = new HashSet<Base>();
    for (Base b : bases)
      fuse(b, lonelyBases);
    return lonelyBases;
  }

  // Inherited Methods (Override or Implementation of abstract)

  protected Name conjunctionBase(Base b) {
    if (this.atom == b.atom &&
        !this.suffix.isEmpty() &&
        !b.suffix.isEmpty() &&
        this.suffix.getPrefix().equals(b.suffix.getPrefix()) &&
        this.suffix.getLast() == Side.RIGHT &&
        b.suffix.getLast() == Side.LEFT) {
      return new Base(this.atom, this.suffix.getPrefix());
    }
    else {
      return new Conjunction(b, this);
    }
  }

  protected boolean equalsConjunction(Conjunction c) {
    return false;
  }

  protected boolean equalsBase(Base b) {
    return this.atom == b.atom && this.suffix.equals(b.suffix);
  }

  protected boolean intersectsConjunction(Conjunction c) {
    return c.getLeft().intersects(this) || c.getRight().intersects(this);
  }

  protected boolean intersectsBase(Base b) {
    return this.atom == b.atom && this.suffix.intersects(b.suffix);
  }

  public Pair<Name, Name> split() {
    Base name1 = new Base(this.atom, this.suffix.append(Side.LEFT));
    Base name2 = new Base(this.atom, this.suffix.append(Side.RIGHT));
    return new Pair<Name, Name>(name1, name2);
  }

  public Name conjunction(Name n) {
    return n.conjunctionBase(this);
  }

  public Name rename(Renaming r) throws NoRenamingException {
    Name newName = r.apply(this.atom);
    return this.suffix.apply(newName); 
  }

  public boolean equals(Name n) {
    return this.equals((Object) n);
  }

  public boolean intersects(Name name) {
    return name.intersectsBase(this);
  }

  public Set<Base> getBases() {
    Set<Base> result =  new HashSet<Base>();
    result.add(this);
    return result;
  }
  
  @Override
  public boolean equals(Object o) {
    return o instanceof Base && ((Base) o).equalsBase(this);
  }

  @Override
  public int hashCode() {
    return 3 * this.atom + 5 * this.suffix.hashCode();
  }
  
  @Override
  public String toString() {
    return this.atom + (this.suffix.isEmpty() ? "" : "." + this.suffix);
  }

}
