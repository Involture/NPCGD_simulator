package npcgd_simulator.name_algebra;

public class Pair<U, V> {

    private U first;
    private V second;

    public Pair(U a, V b){
        this.first = a;
        this.second = b;
    };

    public U getFirst() {
      return this.first;
    };

    public V getSecond() {
      return this.second;
    };
}
