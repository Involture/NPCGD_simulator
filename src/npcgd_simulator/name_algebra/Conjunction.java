package npcgd_simulator.name_algebra;

import java.util.Set;

import npcgd_simulator.name_algebra.exceptions.NoRenamingException;

public class Conjunction extends Name {

  // Fields and Constructors

  private Name left;
  private Name right;

  public Conjunction(Name left, Name right) {
    this.left = left;
    this.right = right;
  }

  // Accessers and Setters

  public Name getLeft() {
    return this.left;
  }

  public Name getRight() {
    return this.right;
  }

  // Class Methods

  // Inherited Methods (Override or Implementation of abstract)

  protected Conjunction conjunctionBase(Base b) {
    return new Conjunction(b, this);
  }

  protected boolean equalsConjunction(Conjunction c) {
    return c.getLeft().equals(this.left) && c.getRight().equals(this.right);
  }

  protected boolean equalsBase(Base b) {
    return false;
  }

  protected boolean intersectsConjunction(Conjunction c) {
    return
      this.left.intersects(c.getLeft()) ||
      this.left.intersects(c.getRight()) ||
      this.right.intersects(c.getLeft()) ||
      this.right.intersects(c.getRight());
  }

  protected boolean intersectsBase(Base b) {
    return this.left.intersectsBase(b) || this.right.intersectsBase(b);
  }

  public Pair<Name, Name> split() {
    return new Pair<Name, Name>(this.left, this.right);
  }

  public Name conjunction(Name n) {
    return new Conjunction(this, n);
  }

  public Name rename(Renaming r) throws NoRenamingException {
    return this.left.rename(r).conjunction(this.right.rename(r));
  }

  public boolean equals(Name name) {
    return name.equalsConjunction(this);
  }

  public boolean intersects(Name name) {
    return name.intersectsConjunction(this);
  }

  public Set<Base> getBases() {
    Set<Base> result = this.left.getBases();
    result.addAll(this.right.getBases());
    return result;
  }
  
  @Override
  public boolean equals(Object o) {
    return o instanceof Conjunction && ((Conjunction) o).equalsConjunction(this);
  }
  
  @Override
  public int hashCode() {
    return 3 * this.left.hashCode() + 5 * this.right.hashCode();
  }
  
  @Override
  public String toString() {
    return String.format("( %s & %s )", this.left, this.right);
  }
}
