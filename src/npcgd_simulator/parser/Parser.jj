options {
  DEBUG_PARSER = false;
  DEBUG_TOKEN_MANAGER = false;
}

PARSER_BEGIN(Parser)

package npcgd_simulator.parser;
import npcgd_simulator.name_algebra.*;
import npcgd_simulator.graphs.*;
import npcgd_simulator.graphs.exceptions.*;
import npcgd_simulator.graphs.normal.*;
import npcgd_simulator.graphs.pattern.*;
import npcgd_simulator.dynamics.*;
import java.util.Map;
import java.util.HashMap;

public class Parser {
}

PARSER_END(Parser)

SKIP :
{
  " "
| "\t"
| "\n"
| "\r"
}

TOKEN :
{
  < SIDE: ("l"|"r") >
| < NUM: ( ["0"-"9"] )+ >
}

int parseInt():
{
  Token t;
}
{
  t = <NUM>
  {
    return Integer.parseUnsignedInt(t.image);
  }
}

Side parseSide() :
{
  Token t;
}
{
  t = <SIDE>
  {
    if (t.image.equals("l")) return Side.LEFT; else return Side.RIGHT;
  }
}

Suffix parseSuffix() :
{
  Suffix result = Suffix.theEnd;
  Side s;
}
{
  ( s = parseSide()
    {
      result = result.append(s);
    }
  )*
  {
    return result;
  }
}

Base parseBase() :
{
  int atom;
  Suffix s = Suffix.theEnd;
}
{
  atom = parseInt()
  (
  "."
  s = parseSuffix()
  )?
  {
    return new Base(atom, s);
  }
}

Conjunction parseConjunction() :
{
  Name n1;
  Name n2;
}
{
  "(" n1 = parseName() "&" n2 = parseName() ")"
  {
    return new Conjunction(n1, n2);
  }
}

Name parseName() :
{
  Name t;
}
{
  (t = parseConjunction() | t = parseBase())
  {
    return t;
  }
}

IntSet parseSimpleSet() :
{
  int i1;
  int i2;
}
{
  (
    i1 = parseInt()
    {
      return new IntSet(i1, 1);
    }
  |
    "[" i1 = parseInt() ";" i2 = parseInt() "]"
    {
      return new IntSet(i1, i2);
    }
  )
}

IntSet parseBoolIntSet() :
{
  IntSet res;
}
{
  (
    res = parseSimpleSet()
  |
    "!" "(" res = parseIntSet() ")"
    {
      res.not();
    }
  )
  {
    return res;
  }
}

IntSet parseIntSet() :
{
  IntSet res;
  IntSet temp;
}
{
  res = parseBoolIntSet()
  (
    "U"
    temp = parseBoolIntSet()
    {
      res.union(temp);
    }
  )*
  {
    return res;
  }
}

PatternVertexStatus parsePatternVertexStatus(Map<Integer, IntSet> sets) :
{
  Name name;
  int setId = -1;
  int singleton = -1;
  IntSet set;
  boolean border = false;
}
{
  name = parseName()
  "("
  (    "S" setId = parseInt()
  |
    singleton = parseInt()
  )
  (
    "*"
    {
      border = true;
    }
  )?
  ")"
  {
    return new PatternVertexStatus(name, setId,
    (setId == -1) ? new IntSet(singleton) : sets.get(setId), border);
  }
}

PatternEdgeStatus parsePatternEdgeStatus(Map<Integer, IntSet> sets) :
{
  int singleton = -1;
  int setId = -1;
  int optionalId = -1;
}
{
  (    "S" setId = parseInt()
  |
    singleton = parseInt()
  )
  (
    "*"
    optionalId = parseInt()
  )?
  {
    return new PatternEdgeStatus(setId,
        (setId == -1) ? new IntSet(singleton) : sets.get(setId),
        optionalId);
  }
}

NormalVertexStatus parseNormalVertexStatus() :
{
  Name name;
  int status;
}
{
  name = parseName() "(" status = parseInt() ")"
  {
    return new NormalVertexStatus(name, status);
  }
}

NormalEdgeStatus parseNormalEdgeStatus() :
{
  int status;
}
{
  status = parseInt()
  {
    return new NormalEdgeStatus(status);
  }
}

void parseNormalVertex(
  Map<Name, Vertex<NormalVertexStatus, NormalEdgeStatus>> m,
  NormalGraph g) :
{
  NormalVertexStatus status;
}
{
  ("v" ":")?
  status = parseNormalVertexStatus()
  {
    m.put(status.getName(), g.addVertex(status));
  }
}

void parsePatternVertex(
  Map<Name, Vertex<PatternVertexStatus, PatternEdgeStatus>> m,
  Map<Integer, IntSet> sets, PatternGraph g) :
{
  PatternVertexStatus status;
}
{
  ("v" ":")?
  status = parsePatternVertexStatus(sets)
  {
    m.put(status.getName(),
      status.isBorder() ? g.addBorderVertex(status) : g.addVertex(status));
  }
}

void parseNormalEdge(Map<Name, Vertex<NormalVertexStatus, NormalEdgeStatus>> m,
    NormalGraph g) :
{
  Name n1;
  Name n2;
  int p1;
  int p2;
  NormalEdgeStatus status;
}
{
  "e" ":"
  n1 = parseName() "|" p1 = parseInt() ">" status = parseNormalEdgeStatus() "<" 
  p2 = parseInt() "|" n2 = parseName()
  {
    try {
      g.addEdge(m.get(n1), m.get(n2), p1, p2, status);
    } catch (NotAPortException ex) {
      throw new ParseException(ex);
    } catch (UsedPortException ex) {
      throw new ParseException(ex);
    }
  }
}

void parsePatternEdge(
  Map<Name, Vertex<PatternVertexStatus, PatternEdgeStatus>> m,
  Map<Integer, IntSet> sets, PatternGraph g) :
{
  Base n1;
  Base n2;
  int p1;
  int p2;
  PatternEdgeStatus status;
}
{
  "e" ":"
  n1 = parseBase() "|" p1 = parseInt() ">"
  (
    status = parsePatternEdgeStatus(sets) "<" 
    p2 = parseInt() "|" n2 = parseBase()
    {
      try { 
        g.addEdge(m.get(n1), m.get(n2), p1, p2, status);
      } catch (NotAPortException ex) {
        throw new ParseException(ex);
      } catch (UsedPortException ex) {
        throw new ParseException(ex);
      }
    }
  |
    "_"
    {
      if (!m.get(n1).getStatus().isBorder())
        throw new ParseException(
            "Non border vertices have empty edges by default." +
            "Please remove this edge.");
      try { 
        if(!m.get(n1).hasEdge(p1))
          throw new ParseException(
              "This port is already set above to have no edge" +
              "Please remove this edge.");
        m.get(n1).unsetEdge(p1);
      } catch (NotAPortException ex) {
        throw new ParseException(ex);
      }
    }
  )
}

NormalGraph parseNormalGraph(int nport) :
{
  NormalGraph g = new NormalGraph(nport);
  Map<Name, Vertex<NormalVertexStatus, NormalEdgeStatus>> m = 
    new HashMap<Name, Vertex<NormalVertexStatus, NormalEdgeStatus>>();
}
{
  (
    parseNormalVertex(m, g)
  |
    parseNormalEdge(m, g)
  )*
  {
    return g;
  }
}

PatternGraph parsePatternGraph(int nport, Map<Integer, IntSet> sets) :
{
  PatternGraph pattern = new PatternGraph(nport);
    
  Map<Name, Vertex<PatternVertexStatus, PatternEdgeStatus>> m = 
    new HashMap<Name, Vertex<PatternVertexStatus, PatternEdgeStatus>>();
}
{
  (
    parsePatternVertex(m, sets, pattern)
  |
    parsePatternEdge(m, sets, pattern)
  )+
  {
    return pattern;
  }
}

NormalGraph parseGraphFile() :
{
  int nport;
  NormalGraph g;
}
{
  nport = parseInt()
  g = parseNormalGraph(nport)
  <EOF>
  {
    return g;
  }
}

Cycle parseCycle(int nport) :
{
  int setId;
  IntSet intSet;
  Map<Integer, IntSet> sets = new HashMap<Integer, IntSet>();
  PatternGraph pattern;
  UnmutableLinkedPattern previous = null;
  UnmutableLinkedPattern image = null;
  Cycle cycle;
}
{
  {
    cycle = new Cycle(nport);
  }
  (    "S" setId = parseInt() "=" intSet = parseIntSet()
    {
      sets.put(setId, intSet);
    }
  )*
  ";"
  (    pattern = parsePatternGraph(nport, sets)
    {
      previous = new UnmutableLinkedPattern(pattern);
      image = previous;
      cycle.addPattern(previous);
    }
  )
  (
    ";"
    (
      pattern = parsePatternGraph(nport, sets)
      {
        previous = new UnmutableLinkedPattern(pattern, previous, image);
        cycle.addPattern(previous);
      }
    )?
  )*
  {
    return cycle;
  }
}

Dynamic parseDynamicFile() :
{
  int nport;
  Dynamic dynamic;
  Cycle cycle;
}
{
  nport = parseInt()
  {
    dynamic = new Dynamic(nport);
  }
  (    cycle = parseCycle(nport)
    {
      dynamic.add(cycle);
    }
  )
  (    "/"
    (      cycle = parseCycle(nport)
      {
        dynamic.add(cycle);
      }
    )?
  )*
  <EOF>
  {
    return dynamic;
  }
} 
  
