package npcgd_simulator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintStream;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.FileConverter;

import npcgd_simulator.dynamics.Dynamic;
import npcgd_simulator.graphs.normal.NormalGraph;
import npcgd_simulator.parser.ParseException;
import npcgd_simulator.parser.Parser;

public class Main {
    
  @Parameter(names = {"-g", "--graph"}, required = true,
      converter = FileConverter.class)
  File graphFile;
  
  @Parameter(names = {"-d", "--dynamic"}, required = true, 
      converter = FileConverter.class)
  File dynamicFile;
  
  @Parameter(names = {"-s, --steps"})
  int steps = 1;
  
  @Parameter(names = {"-f, --fileoutput"})
  boolean fileoutput = false;
  
  @Parameter(names = {"-d, --outputdirectory"}, 
      converter = FileConverter.class)
  File outputDirectory;
  
  @Parameter(names = {"-o, --outputformat"})
  String outputformat = "human";
  
  @Parameter(names = {"-l, --lastonly"})
  boolean lastonly = false;
  
  int step;
  String outputFileName;
    
  Main(String[] argv) {
	JCommander.newBuilder().addObject(this).build().parse(argv);
    this.outputFileName = 
        dynamicFile.getName() + "(" + graphFile.getName() + ")";
    if (this.outputDirectory == null)
      this.outputDirectory = this.graphFile.getParentFile();
  }
  
  public static void main (String[] argv)
    throws FileNotFoundException, ParseException {
	Main main = new Main(argv);
    main.run();
  }
  
  static {
    new Parser(System.in);
  }
  
  private void parserInit(File file) 
      throws FileNotFoundException {
    Parser.ReInit(new FileReader(file));
  }
  
  private PrintStream getOutputStream()
    throws FileNotFoundException {
	if (!fileoutput)
      return System.out;
    return new PrintStream(
        new File(this.outputDirectory, this.outputFileName + this.step)
        );
  }
  
  private void output(NormalGraph g) 
    throws FileNotFoundException {
    PrintStream out = this.getOutputStream();
    if (outputformat.equals("human")) {
      out.println(g);
      return;
    }
    throw new IllegalArgumentException(
        "Unsupported output format : " + outputformat);
  }
  
  public void run() 
    throws FileNotFoundException, ParseException {
    parserInit(graphFile);
    NormalGraph g = Parser.parseGraphFile();
    parserInit(dynamicFile);
    Dynamic d = Parser.parseDynamicFile();
    for(this.step = 0; this.step <= this.steps; this.step++) {
      output(g);
      d.apply(g);
    }
  }
  
}
