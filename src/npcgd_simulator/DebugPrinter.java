package npcgd_simulator;

public class DebugPrinter {
  static int debugLevel = -1;
  
  public static int setLevel(int level) {
	int oldLevel = debugLevel;
    if (level >= -1)
      debugLevel = level;
    return oldLevel;
  }
  
  public static boolean print(int level, String message) {
	boolean condition = level <= debugLevel || debugLevel == -1;
    if (condition)
      System.out.println(message);
    return condition;
  }
  
}
