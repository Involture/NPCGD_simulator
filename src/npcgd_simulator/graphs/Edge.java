package npcgd_simulator.graphs;

import npcgd_simulator.graphs.exceptions.NotAPortException;
import npcgd_simulator.graphs.exceptions.UsedPortException;

// Property
// 1) An edge links two distinct vertices of the same graph.
// 2) The edge is referenced in the two vertices it leads to.
public class Edge<VT extends Matchable, ET extends Matchable> {

  private Vertex<VT, ET> vertex1;
  private Vertex<VT, ET> vertex2;
  private int port1;
  private int port2;
  private ET status;
  
  @SuppressWarnings("rawtypes")
protected static Edge<?, ?> border = new Edge();

  private Edge() {
	this.vertex1 = null;
	this.vertex2 = null;
	this.port1 = -1;
	this.port2 = -1;
	this.status = null;
  }
  
  // Requires that vertex1 and vertex2 are two (possibly equals) vertices of
  // a same graph to ensure property 1.
  // Ensure property 2 by calling firstSet and secondSet.
  protected Edge(Vertex<VT, ET> vertex1, 
              Vertex<VT, ET> vertex2, 
      int port1, int port2, ET status) 
      throws UsedPortException, NotAPortException {
    this.status = status;
    this.firstSet(vertex1, port1);
    this.secondSet(vertex2, port2);
  }

  public boolean isBorder() {
    return this == Edge.border;
  }
  
  protected Vertex<VT, ET> getVertex1() {
    assert(!this.isBorder());
    return vertex1;
  }

  protected Vertex<VT, ET> getVertex2() {
    assert(!this.isBorder());
    return vertex2;
  }

  protected int getPort1() {
    assert(!this.isBorder());
    return port1;
  }

  protected int getPort2() {
    assert(!this.isBorder());
    return port2;
  }
  
  public int getPort(Vertex<VT, ET> v) {
    assert(!this.isBorder());
    if (this.vertex1 == v)
      return this.port1;
    if (this.vertex2 == v)
      return this.port2;
    return -1;
  }

  public ET getStatus() {
    assert(!this.isBorder());
    return status;
  }

  private void firstSet(Vertex<VT, ET> v, int p)
      throws UsedPortException, NotAPortException {
    assert(!this.isBorder());
    this.vertex1 = v;
    this.vertex1.setEdge(p, this);
    this.port1 = p;
  }

  private void secondSet(Vertex<VT, ET> v, int p)
      throws UsedPortException, NotAPortException {
    assert(!this.isBorder());
    this.vertex2 = v;
    this.vertex2.setEdge(p, this);
    this.port2 = p;
  }

  public void delete () {
    try {
      this.vertex1.unsetEdge(this.port1);
      if (!this.isBorder())
        this.vertex2.unsetEdge(this.port2);
    } catch (NotAPortException ex) {
      throw new Error("Cannot happen", ex);
    }
  } 

  @Override
  public String toString() {
    return this.vertex1.getStatus() + " | " + this.port1 + " > " + 
        this.status + " < " + this.port2 + " | " + this.vertex2.getStatus();
  }

}
