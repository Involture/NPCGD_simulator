package npcgd_simulator.graphs.normal;

import npcgd_simulator.graphs.Matchable;

// Properties
// 1) Status is positive.
public class NormalEdgeStatus implements Matchable {

  private int status;

  // Ensure property 1 by throwing exception.
  public NormalEdgeStatus(int status) {
	if (status < 0)
	  throw new IllegalArgumentException(status +
	      ": Argument must be positive");
    this.status = status;
  }

  public int getStatus() {
    return status;
  }

  public boolean equals(NormalEdgeStatus s) {
    return this.status == s.getStatus();
  }
  
  @Override
  public String toString() {
    return this.status + "";
  }

}
