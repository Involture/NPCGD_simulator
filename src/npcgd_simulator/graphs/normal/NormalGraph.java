package npcgd_simulator.graphs.normal;

import npcgd_simulator.graphs.Graph;
import npcgd_simulator.graphs.Vertex;
import npcgd_simulator.name_algebra.Name;

// Properties
// 1) There is no two names intersecting

public class NormalGraph extends Graph<NormalVertexStatus, NormalEdgeStatus> {

  public NormalGraph(int nport) {
    super(nport);
  }
  
  // Override to ensure property 1
  @Override()
  public Vertex<NormalVertexStatus, NormalEdgeStatus> addVertex(NormalVertexStatus status) {
    Name n = status.getName();
    for (Vertex<NormalVertexStatus, NormalEdgeStatus> u : this.getVertices()) {
      Name m = u.getStatus().getName();
      assert(!m.intersects(n));
    }
    return super.addVertex(status);
  }
  
}
