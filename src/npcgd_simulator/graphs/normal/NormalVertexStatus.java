package npcgd_simulator.graphs.normal;

import npcgd_simulator.graphs.Matchable;
import npcgd_simulator.name_algebra.Name;

// Properties
// 1) status is positive
public class NormalVertexStatus implements Matchable {

  private Name name;
  private int status;
  
  // Ensure property 1 by throwing exception.
  public NormalVertexStatus(Name name, int status) {
    this.name = name;
    if (status < 0)
      throw new IllegalArgumentException(status + 
          ": Argument must be positive");
    this.status = status;
  }

  public Name getName() {
    return name;
    }

  public int getStatus() {
    return status;
  }

  public boolean equals(NormalVertexStatus s) {
    return this.name.equals(s.getName()) && this.status == s.getStatus();
  }

  @Override
  public String toString() {
    return name + "(" + status + ")";
  }
  
}
