package npcgd_simulator.graphs;

public interface Matchable {

  default boolean matches(Matchable m) {
    return false;
  }

}
