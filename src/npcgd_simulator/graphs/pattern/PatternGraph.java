package npcgd_simulator.graphs.pattern;

import java.util.HashSet;
import java.util.Set;

import npcgd_simulator.graphs.Graph;
import npcgd_simulator.graphs.Vertex;
import npcgd_simulator.graphs.exceptions.NotAPortException;
import npcgd_simulator.graphs.exceptions.UsedPortException;
import npcgd_simulator.name_algebra.Base;
import npcgd_simulator.name_algebra.Name;

// Properties
// 1) There is no two names intersecting.
// 2) Only border vertices have border edges.
public class PatternGraph 
  extends Graph<PatternVertexStatus, PatternEdgeStatus> {

  // Constructors
	
  public PatternGraph(int nport) {
    super(nport);
  }
  
  protected PatternGraph(PatternGraph pattern) {
    super(pattern);
  }
  
  // Getters
  
  public Set<Vertex<PatternVertexStatus, PatternEdgeStatus>> getBorder() {
	Set<Vertex<PatternVertexStatus, PatternEdgeStatus>> result =
	  new HashSet<Vertex<PatternVertexStatus, PatternEdgeStatus>>();
    for (Vertex<PatternVertexStatus, PatternEdgeStatus> v : this.getVertices())
      if (v.getStatus().isBorder())
        result.add(v);
    return result;
  }

  public Set<Base> getBases() {
    Set<Base> set = new HashSet<Base>();
    for(PatternVertexStatus s : this.getVerticesStatus())
      set.addAll(s.getName().getBases());
    return set;
  }
  
  public Set<Base> getLonelyBases() {
    return Base.LonelyBases(this.getBases());
  }
  
  public Set<Integer> getAtoms() {
    Set<Integer> atoms = new HashSet<Integer>();
    for (Base b : this.getBases())
      atoms.add(b.getAtom());
    return atoms;
  }
  
  // Overrides to ensure properties
  
  // Ensure property 1
  @Override()
  public Vertex<PatternVertexStatus, PatternEdgeStatus> 
      addVertex(PatternVertexStatus status) {
	Name n = status.getName();
    for (Vertex<PatternVertexStatus, PatternEdgeStatus> u : 
        this.getVertices()) {
      Name m = u.getStatus().getName();
      assert(!m.intersects(n));
    }
    return super.addVertex(status);
  }
  
  // Ensure property 2
  @Override()
  public void addBorder(Vertex<PatternVertexStatus, PatternEdgeStatus> v, 
      int port) 
      throws UsedPortException, NotAPortException {
    assert(v.getStatus().isBorder());
    super.addBorder(v, port);
  }
  
  public Vertex<PatternVertexStatus, PatternEdgeStatus> 
    addBorderVertex(PatternVertexStatus status) {
	assert(status.isBorder());
    Vertex<PatternVertexStatus, PatternEdgeStatus> result = 
      this.addVertex(status);
    try {
      for(int i = 0; i < this.getNport(); i++) {
        this.addBorder(result, i);
      }
    } catch (UsedPortException e) {
      assert(false);
    } catch (NotAPortException e) {
      assert(false);
    }
    return result;
  }
 
}
