package npcgd_simulator.graphs.pattern;

import npcgd_simulator.graphs.Matchable;
import npcgd_simulator.graphs.normal.NormalVertexStatus;
import npcgd_simulator.name_algebra.Name;

// Property
// 1) setId is either positive or equals to -1
// 2) If setId equals -1, et is a singleton.
public class PatternVertexStatus implements Matchable {
  
  private Name name;
  private int setId;
  private IntSet set;
  private boolean border;

  
  // Ensure properties 1 and 2 by throwing exceptions.
  public PatternVertexStatus(Name name, int setId, 
      IntSet set, boolean border) {
	if (setId < 0)
	  if (setId == -1) {
	    if (!set.isSingleton())
	      throw new IllegalArgumentException(set +
	          ": Set must be a singleton as it has no setId.");
	  }
	  else {
	    throw new IllegalArgumentException(setId +
	        ": SetId must either be positive or equals -1");
	  }
    this.name = name;
    this.setId = setId;
    this.set = set;
    this.border = border;
  }

  public Name getName() {
    return name;
  }

  public int getSetId() {
    return setId;
  }

  public IntSet getSet() {
    return set;
  }

  public boolean isBorder() {
    return border;
  }
  
  public boolean matches(Matchable obj) {
    if (obj instanceof NormalVertexStatus)
      return this.matches((NormalVertexStatus) obj);
    if (obj instanceof PatternVertexStatus)
      return this.matches((PatternVertexStatus) obj);
    return false;
  }

  public boolean matches(PatternVertexStatus s) {
    if (s == null)
      return true;
    IntSet inter = new IntSet(this.set);
    inter.intersect(s.getSet());
    return !inter.isEmpty() && !(this.border && s.isBorder());
  }

  public boolean matches(NormalVertexStatus s) {
    return this.set.contains(s.getStatus());
  }
  
  public boolean myEquals(PatternVertexStatus s) {
    return this.name.equals(s.getName()) &&
        this.set.equals(s.getSet()) &&
        this.setId == s.getSetId() &&
        !(this.isBorder() ^ s.isBorder());
  }

  @Override
  public String toString() {
    return this.name + "(" + 
        (this.setId == -1 ? this.set.getSingleton() : 
            "S" + this.setId + "=" + this.set) +
        (this.border ? "*" : "") + ")";
  }
  
  
}
