package npcgd_simulator.graphs.pattern;

import npcgd_simulator.graphs.Matchable;
import npcgd_simulator.graphs.normal.NormalEdgeStatus;



// Properties
// 1) setId is either positive or equals to -1
// 2) If setId equals -1, et is a singleton.
public class PatternEdgeStatus implements Matchable {

  private int setId;
  private IntSet set;
  private int optionalId;

  public PatternEdgeStatus(int setId, IntSet set, int optionalId) {
	if (setId < 0)
	  if (setId == -1) {
	    if (!set.isSingleton())
	      throw new IllegalArgumentException(set +
	          ": Set must be a singleton as it has no setId.");
	  }
	  else {
	    throw new IllegalArgumentException(setId +
	        ": SetId must either be positive or equals -1");
	  }
    this.setId = setId;
    this.set = set;
    this.optionalId = optionalId;
  }

  public int getSetId() {
    return setId;
  }

  public IntSet getSet() {
    return set;
  }

  public int getOptionalId() {
    return optionalId;
  }

  public boolean isOptional() {
    return optionalId != -1;
  }

  public boolean equals(PatternEdgeStatus s) {
    return this.set.equals(s.getSet()) &&
      (this.isOptional() ^ s.isOptional());
  }

  public boolean matches(Matchable s) {
    if (s instanceof PatternEdgeStatus)
      return this.matches((PatternEdgeStatus) s);
    if (s instanceof NormalEdgeStatus)
      return this.matches((NormalEdgeStatus) s);
    return false;
  }
  
  public boolean matches(PatternEdgeStatus s) {
    if (s == null)
      return this.isOptional();
    IntSet inter = new IntSet(this.set);
    inter.intersect(s.getSet());
    return !inter.isEmpty();
  }

  public boolean matches(NormalEdgeStatus s) {
    if (s == null)
      return this.isOptional();
    return this.set.contains(s.getStatus());
  }
  
  @Override
  public String toString() {
    return (this.setId == -1 ? this.set.getSingleton() : 
        "S" + this.setId + "=" + this.set) +
        (this.isOptional() ? "*" + this.optionalId : "");
  }

}
