package npcgd_simulator.graphs.pattern;

import npcgd_simulator.name_algebra.Name;

@SuppressWarnings("serial")
public class NameNotFoundException extends Exception {
	
  NameNotFoundException(PatternGraph g, Name name) {
    super(String.format("Name %s was not found in graph",  name));
  }
	
}
