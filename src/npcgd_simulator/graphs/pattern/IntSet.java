package npcgd_simulator.graphs.pattern;

import java.util.TreeSet;

public class IntSet {
  
  private TreeSet<Integer> bounds;
  private boolean containsZero;

  public IntSet() {
    this.bounds = new TreeSet<Integer>();
    this.containsZero = false;
  }

  public IntSet(int lower, int size) {
    this.bounds = new TreeSet<Integer>();
    if (lower != 0) this.bounds.add(lower);
    this.bounds.add(lower + size);
    this.containsZero = (lower == 0);
  }
  
  public IntSet(int singleton) {
    this(singleton, 1);
  }

  @SuppressWarnings("unchecked")
  public IntSet(IntSet s) {
    this.bounds = (TreeSet<Integer>) s.getBounds().clone();
    this.containsZero = s.containsZero();
  }

  public TreeSet<Integer> getBounds() {
    return bounds;
  }

  public boolean containsZero() {
    return containsZero;
  }

  public boolean isEmpty() {
    return !this.containsZero && bounds.isEmpty();
  }
  
  public int getSize() {
	int result = 0;
	int previousBound = this.containsZero ? 0 : -1;
	for (int bound : this.bounds) {
      if (!(previousBound == -1)) {
    	result += bound - previousBound;
    	previousBound = -1;
      }
      else {
    	previousBound = bound;
      }
	}
	if (previousBound == -1)
	  return result;
	return -1;
  }

  public boolean isSingleton() {
	return (this.getSize() == 1);
  }
  
  public int getSingleton() {
	if (this.isSingleton()) {
      if (this.containsZero)
        return 0;
	  return this.bounds.iterator().next();
	}
	return -1;
  }
  
  public void not() {
    this.containsZero = !this.containsZero;
  }

  public void union(IntSet s) {
    TreeSet<Integer> result = new TreeSet<Integer>();
    TreeSet<Integer> unionSet = new TreeSet<Integer>();
    unionSet.addAll(this.bounds);
    unionSet.addAll(s.bounds);
    boolean belongsThis = this.containsZero;
    boolean belongsThat = s.containsZero;
    boolean belongs = belongsThis | belongsThat;
    for (Integer i : unionSet) {
      if (this.bounds.contains(i)) belongsThis = !belongsThis;
      if (s.bounds.contains(i)) belongsThat = !belongsThat;
      if (belongs ^ (belongsThat | belongsThis)) result.add(i);
      belongs = belongsThat | belongsThis;
    }
    this.bounds = result;
    this.containsZero |= s.containsZero;
  }

  public void intersect(IntSet s) {
    this.not();
    s = new IntSet(s);
    s.not();
    this.union(s);
    this.not();
  }

  public boolean contains(int i) {
    Integer ii = Integer.valueOf(i);
    int switchn = this.bounds.headSet(ii + 1).size();
    int sw = switchn % 2;
    return this.containsZero ^ (sw == 1);
  }

  public boolean equals(IntSet s) {
    return this.bounds.equals(s.getBounds()) && 
      !(this.containsZero ^ s.containsZero());
  }
  
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    boolean in = this.containsZero;
    boolean first = true;
    if (this.containsZero) {
      builder.append("[0;");
      first = false;
    }
    for (Integer bound : this.bounds) {
      if (in) {
        builder.append(bound + "[");
      }
      else {
    	if (!first) {
          builder.append("U");
    	}
    	else {
          first = false;
    	}
        builder.append("[" + bound + ";");
      }
      in = !in;
    }
    if (in)
      builder.append("inf[");
    return builder.toString();
  }

}
