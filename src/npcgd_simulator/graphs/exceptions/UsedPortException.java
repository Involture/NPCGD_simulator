package npcgd_simulator.graphs.exceptions;
import npcgd_simulator.graphs.Vertex;

@SuppressWarnings("serial")
public class UsedPortException extends Exception {

  public UsedPortException(Vertex<?, ?> v, int i) {
    super(String.format("Vertex %s has already an edge on port %d", v, i));
  }

}
