package npcgd_simulator.graphs.exceptions;

@SuppressWarnings("serial")
public class NotAPortException extends Exception {

  public NotAPortException(int i, int nport) {
    super(String.format(
            "Port %d does not exist. Number of port is %d", i, nport - 1));
  }

}
