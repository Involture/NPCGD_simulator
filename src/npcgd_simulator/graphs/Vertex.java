package npcgd_simulator.graphs;

import java.lang.reflect.Array;
import java.util.HashSet;
import java.util.Set;

import npcgd_simulator.graphs.exceptions.NotAPortException;
import npcgd_simulator.graphs.exceptions.UsedPortException;

// Property
// 1) A vertex is referenced in one and only one graph
// 2) Any edges of the vertex leads to a vertex contained in the same graph.

public class Vertex<VT extends Matchable, ET extends Matchable> 
  implements Matchable {

  private VT status;
  private Edge<VT, ET>[] edges;

  @SuppressWarnings("unchecked")
  // Ensure property 1 as the new vertex has no edge
  // To ensure property 2, this constructor must be called only when
  // adding a vertex to a graph.
  protected Vertex(VT status, int nport) {
    this.status = status;
    this.edges = (Edge<VT, ET>[]) new Edge<?, ?>[nport] ;
  }

  public VT getStatus() {
    return status;
  }
  

  public Set<Edge<VT, ET>> getEdges() {
    Set<Edge<VT, ET>> result = new HashSet<Edge<VT, ET>>();
    for (int i = 0; i < this.getNport(); i++)
      if (this.edges[i] != null)
        result.add(this.edges[i]);
    return result;
  }
  
  protected int getNport() {
    return Array.getLength(this.edges);
  }

  public Edge<VT, ET> getEdge(int i) throws NotAPortException {
    try {
      return this.edges[i];
    } 
    catch (IndexOutOfBoundsException e) {
      throw new NotAPortException(i, Array.getLength(this.edges));
    }
  }

  public boolean hasEdge(int i) throws NotAPortException {
    return this.getEdge(i) != null;
  }
  
  public Set<Integer> getUsedPorts() {
	Set<Integer> result = new HashSet<Integer>();
	for (int i = 0; i < this.getNport(); i++)
	  if (this.edges[i] != null && this.edges[i] != Edge.border)
	    result.add(Integer.valueOf(i));
	return result;
  }
  
  public Set<Integer> getFreePorts() {
	Set<Integer> result = new HashSet<Integer>();
	for (int i = 0; i < this.getNport(); i++)
	  if (this.edges[i] == null)
	    result.add(Integer.valueOf(i));
	return result;
  }
  
  public Set<Integer> getBorderPorts() {
	Set<Integer> result = new HashSet<Integer>();
	for (int i = 0; i < this.getNport(); i++)
	  if (this.edges[i] == Edge.border)
	    result.add(Integer.valueOf(i));
	return result;
  }

  // Keeps property 1.
  // Require that the edge links two vertices of the same graph to ensure
  // property 2.
  protected void setEdge(int p, Edge<VT, ET> e) 
      throws UsedPortException, NotAPortException {
    if (this.hasEdge(p) && !this.getEdge(p).isBorder()) {
      throw new UsedPortException(this, p);
    }
    else {
      try {
        this.edges[p] = e;
      }
      catch (IndexOutOfBoundsException ex) {
        throw new NotAPortException(p, this.getNport());
      }
    }
  }
  
  public void unsetEdge(int p)
      throws NotAPortException {
    if (!this.hasEdge(p))
      throw new IllegalArgumentException("Vertex " + this + 
          " has no edge on port " + p);
    try {
      this.edges[p] = null;
    } catch (IndexOutOfBoundsException ex) {
      throw new NotAPortException(p, this.getNport());
    }
  }

  public Vertex<VT, ET> getNeighboor(int port) throws NotAPortException {
    Edge<VT, ET> e = this.getEdge(port);
    if (e == null || e.isBorder())
      throw new IllegalArgumentException(port + ": Vertex " + this + "has no edge this port");
    if (e.getVertex1() == this)
      return e.getVertex2();
    return e.getVertex1();
  }

  protected <AT extends Matchable, BT extends Matchable> 
      boolean matches(Vertex<AT, BT> v) {
    return this.status.matches(v.getStatus());
  }
  
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder(this.status + "\n");
    try {
      for (int port = 0; port < this.getNport(); port ++) {
    	if (!this.hasEdge(port))
    	  continue;
        Edge<VT, ET> e = this.getEdge(port);
        if (!e.isBorder()) {
          Vertex<VT, ET> n = this.getNeighboor(port);

          builder.append(port + " > " + e.getStatus() + " < " + e.getPort(n) + 
              " | " + n.getStatus() + "\n");
        }
        else {
          builder.append(port + " > ***\n");
        }
      }
    } catch (NotAPortException exn) {
      throw new Error("cannot happen");
    }
    return builder.toString();
  }

}
