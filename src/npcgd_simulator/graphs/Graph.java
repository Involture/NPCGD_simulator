package npcgd_simulator.graphs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import npcgd_simulator.graphs.exceptions.NotAPortException;
import npcgd_simulator.graphs.exceptions.UsedPortException;

public class Graph<VT extends Matchable, ET extends Matchable> { 

  private Set<Vertex<VT, ET>> vertices;
  private int nport;
  private Vertex<VT, ET> start;

  public Graph(int nport) {
    this.vertices = new HashSet<Vertex<VT, ET>>();
    this.nport = nport;
  }
  
  protected Graph(Graph<VT, ET> g) {
    this.vertices = new HashSet<Vertex<VT, ET>>();
    this.nport = g.getNport();
    Map<Vertex<VT, ET>, Vertex<VT, ET>> replacement =
      new HashMap<Vertex<VT, ET>, Vertex<VT, ET>>();
    for (Vertex<VT,ET> v : g.getVertices()) {
      Vertex<VT, ET> newV = new Vertex<VT, ET> (v.getStatus(), g.getNport());
      if (this.start == null)
        start = newV;
      for (Integer i : v.getBorderPorts())
    	try {
          this.addBorder(newV, i);
    	} catch (NotAPortException ex) {
          assert(false);
    	} catch (UsedPortException ex) {
    	  assert(false);
    	}
      this.vertices.add(newV);
      replacement.put(v, newV);
    }
    for (Edge<VT, ET> e : g.getEdges())
      try {
    	if (!e.isBorder()) {
          new Edge<VT, ET>(
              replacement.get(e.getVertex1()),
              replacement.get(e.getVertex2()),
              e.getPort1(),e.getPort2() , e.getStatus());
    	}
      } catch (NotAPortException exn) {
        throw new Error("cannot happen.");
      } catch (UsedPortException exn) {
        throw new Error("cannot happen");
      }
  }

  public Set<Vertex<VT, ET>> getVertices() {
    return vertices;
  }
  
  public int getNport() {
	return this.nport;
  }
  
  public Vertex<VT, ET> getStart() {
    if (this.start == null)
      throw new UnsupportedOperationException(
          "Cannot take start of empty graph");
    return this.start;
  }

  public Set<Edge<VT, ET>> getEdges() {
    Set<Edge<VT,ET>> result = new HashSet<Edge<VT,ET>>();
    for (Vertex<VT, ET> v : this.vertices)
      for (Edge<VT, ET> e : v.getEdges())
        if (!result.contains(e))
          result.add(e);
    return result;
  }

  public Set<VT> getVerticesStatus() {
    Set<VT> set = new HashSet<VT>();
    for (Vertex<VT, ET> v : this.vertices)
      set.add(v.getStatus());
    return set;
  }
  
  public boolean isEmpty() {
    return this.vertices.isEmpty();
  }

  private void addVertex(Vertex<VT, ET> v)  {
    if (this.start == null)
      this.start = v;
    this.vertices.add(v);
  }

  public Vertex<VT, ET> addVertex(VT status) {
    Vertex<VT, ET> v = new Vertex<VT, ET>(status, nport);
    this.addVertex(v);
    return v;
  }
  
  // Fills Edge constructor requirements by throwing errors.
  public Edge<VT, ET> addEdge(Vertex<VT, ET> v1, Vertex<VT, ET> v2,
      int p1, int p2, ET status)
      throws UsedPortException, NotAPortException {
	if (!this.vertices.contains(v1))
	  throw new IllegalArgumentException(v1 + 
	      " is not contained in the graph");
	if (!this.vertices.contains(v2))
	  throw new IllegalArgumentException(v2 +
	      " is not contained in the graph");
	return new Edge<VT, ET>(v1, v2, p1, p2, status);
  }
  
  protected void addBorder(Vertex<VT, ET> v, int p) 
      throws UsedPortException, NotAPortException {
    @SuppressWarnings("unchecked")
	Edge<VT, ET> border = (Edge<VT, ET>) Edge.border;
	v.setEdge(p, border);
  }

  public boolean deleteVertex(Vertex<VT, ET> v) {
    boolean removed = this.vertices.remove(v);
    for (Edge<VT, ET> e : v.getEdges())
      e.delete();
    return removed;
  }

  private <AT extends Matchable, BT extends Matchable> boolean tryMatchEdge(
      Vertex<VT, ET> start1, Vertex<AT, BT> start2,
      Map<Vertex<VT, ET>, Vertex<AT, BT>> matching, int port) 
      throws NotAPortException {
    if (!start1.hasEdge(port))
      return !start2.hasEdge(port) || start2.getEdge(port).isBorder();
    Edge<VT, ET> edge1 = start1.getEdge(port);
    if (edge1.isBorder())
      return true;
    
    ET status1 = edge1.getStatus();
    BT status2;
    
    if (start2.hasEdge(port)) {
      Edge<AT, BT> edge2 = start2.getEdge(port);
      if (edge2.isBorder())
        return true;
      status2 = edge2.getStatus();
      if(!status1.matches(status2))
        return false;
      if (!tryMatch(start1.getNeighboor(port), start2.getNeighboor(port), 
          matching))
    	return false;
      return true;
    }
    else {
      status2 = null;
      return status1.matches(status2);
    }
  }
  
  private <AT extends Matchable, BT extends Matchable> boolean tryMatch(
      Vertex<VT, ET> start1, Vertex<AT, BT> start2, 
      Map<Vertex<VT, ET>, Vertex<AT, BT>> matching) {
    if (matching.containsKey(start1))
      return matching.get(start1) == start2;
    if (!start1.matches(start2))
      return false;
    matching.put(start1, start2);
    
    boolean fullMatch = true;
    try {
      for (int i = 0; i < nport; i++) {
        fullMatch &= tryMatchEdge(start1, start2, matching, i);
      }
    }
    catch (NotAPortException e) {
      throw new Error("Cannot happen");
    }
    return fullMatch;
  }

  public <AT extends Matchable, BT extends Matchable>
      Set<Map<Vertex<VT, ET>, Vertex<AT, BT>>>
      matches (Graph<AT, BT> g) {
    Set<Map<Vertex<VT, ET>, Vertex<AT, BT>>> matchings =
      new HashSet<Map<Vertex<VT, ET>, Vertex<AT, BT>>>();
    Map<Vertex<VT, ET>, Vertex<AT, BT>> matching =
      new HashMap<Vertex<VT, ET>, Vertex<AT, BT>>();
    if (this.vertices.isEmpty())
      return matchings;
    Vertex<VT, ET> start = this.vertices.iterator().next();
    for (Vertex<AT, BT> v : g.getVertices()) {
      if (this == g && start == v)
        continue;
      matching.clear();
      if (tryMatch(start, v, matching)) {
        matchings.add(matching);
        matching = new HashMap<Vertex<VT, ET>, Vertex<AT, BT>>();
      }
    }
    return matchings;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    for (Vertex<VT, ET> v : this.vertices)
      builder.append(v.getStatus() + "\n");
    for (Edge<VT, ET> e : this.getEdges())
      if (!e.isBorder())
        builder.append(e + "\n");
    return builder.toString();
  }
  
}
