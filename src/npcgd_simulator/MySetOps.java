package npcgd_simulator;

import java.util.HashSet;
import java.util.Set;

public abstract class MySetOps {

  public static <T> boolean contains(Set<T> set1, Set<T> set2) {
	Set<T> diff = new HashSet<T>(set2);
    diff.removeAll(set1);
    return diff.isEmpty();
  }
  
  public static <T> Set<T> inter(Set<T> set1, Set<T> set2) {
    Set<T> inter = new HashSet<T>(set2);
    inter.retainAll(set1);
    return inter;
  }
  
  public static <T> Set<T> union(Set<T> set1, Set<T> set2) {
    Set<T> union = new HashSet<T>(set1);
    union.addAll(set2);
    return union;
  }
  
  public static Set<Integer> not(Set<Integer> set, int bound) {
    Set<Integer> result = new HashSet<Integer>();
    for (int i = 0; i < bound; i++)
      result.add(i);
    result.removeAll(set);
    return result;
  }
	
}
