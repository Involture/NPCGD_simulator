package npcgd_simulator.dynamics.exceptions;

import npcgd_simulator.graphs.Vertex;
import npcgd_simulator.graphs.pattern.PatternEdgeStatus;
import npcgd_simulator.graphs.pattern.PatternVertexStatus;

@SuppressWarnings("serial")
public class BorderPortUsedException extends Exception {
  
  protected BorderPortUsedException(
      Vertex<PatternVertexStatus, PatternEdgeStatus> v, int port) {
    super("Border vertex " + v + "may be linked to the rest of the graphe" +
        "through port " + port + ". Please specify that this port" + 
        "must not be linked if you want to use it.");
  }
	
}
