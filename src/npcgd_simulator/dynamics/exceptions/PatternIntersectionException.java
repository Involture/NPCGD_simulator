package npcgd_simulator.dynamics.exceptions;

import java.util.Map;

import npcgd_simulator.graphs.Vertex;
import npcgd_simulator.graphs.pattern.PatternEdgeStatus;
import npcgd_simulator.graphs.pattern.PatternGraph;
import npcgd_simulator.graphs.pattern.PatternVertexStatus;

@SuppressWarnings("serial")
public class PatternIntersectionException extends Exception {

  public PatternIntersectionException(
      PatternGraph pattern1, PatternGraph pattern2,
      Map<Vertex<PatternVertexStatus, PatternEdgeStatus>,
      Vertex<PatternVertexStatus, PatternEdgeStatus>> matching) {
    super("\n" + pattern1 + "intersects\n" + pattern2);
  }

}
