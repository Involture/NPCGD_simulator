package npcgd_simulator.dynamics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import npcgd_simulator.graphs.normal.NormalGraph;


// Properties
// 1) Every linked pattern 'counterImage' pattern is the previous one in the
// pattern list, cycling.
// 2) There is no two patterns that intersect.

public class Cycle {
  
  private int nport;
  private List<UnmutableLinkedPattern> patterns;

  // Properties ensured because everything is empty
  public Cycle(int nport) {
    this.nport = nport;
    this.patterns = new ArrayList<UnmutableLinkedPattern>();
  }

  public List<UnmutableLinkedPattern> getPatterns() {
    return this.patterns;
  }

  public int getNport() {
    return nport;
  }
  
  public int getSize() {
    return this.patterns.size();
  }

  private UnmutableLinkedPattern modGet(int index) {
    return patterns.get(index % patterns.size());
  }
  
  private void intersectionCheck(UnmutableLinkedPattern pattern) {
    for (UnmutableLinkedPattern previousPattern : patterns)
      pattern.checkIntersection(previousPattern);
  }
  
  // Property 1 ensured by assertion check and relink.
  // Property 2 ensured thanks to intersection check.
  public void addPattern(UnmutableLinkedPattern pattern) {
    this.intersectionCheck(pattern);
    if (this.getSize() == 0) {
      assert (pattern.getCounterImage() == pattern);
      assert (pattern.getImage() == pattern);
    }
    else {
      assert (pattern.getCounterImage() == this.modGet(-1));
      assert (pattern.getImage() == this.modGet(0));
    }
	this.patterns.add(pattern);
  }
  
  public void apply(NormalGraph target) {
	List<Collection<PatternMatching>> matchingsCollections =
      new ArrayList<Collection<PatternMatching>>();
    for(UnmutableLinkedPattern pattern : patterns)
      matchingsCollections.add(pattern.getMatches(target));
    for(int i = 0; i < this.getSize() ; i ++)
      this.modGet(i).applyMatchings(matchingsCollections.get(i));
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    for (UnmutableLinkedPattern pattern : patterns) {
      builder.append(" |\n v\n" + pattern);
    }
    builder.append(" |<_\n |__|\n");
    return builder.toString();
  }
  
}
