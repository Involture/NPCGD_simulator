package npcgd_simulator.dynamics;

import java.util.ArrayList;
import java.util.List;

import npcgd_simulator.graphs.normal.NormalGraph;

public class Dynamic {

  private List<Cycle> cycles;
  private int nport;
	
  public Dynamic(int nport) {
    this.nport = nport;
    this.cycles = new ArrayList<Cycle>(); 
  }
  
  public void add(Cycle cycle) {
    if (cycle.getNport() != this.nport)
      throw new IllegalArgumentException(cycle + ": Every cycle must have the same number of port");
    cycles.add(cycle);
  }
  
  public void apply(NormalGraph target) {
    for (Cycle cycle : cycles)
      cycle.apply(target);
  }
  
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    for (Cycle cycle : cycles) {
      builder.append(cycle + "/\n");
    }
    return builder.toString();
  }
	
}
