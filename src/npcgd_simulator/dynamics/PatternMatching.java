package npcgd_simulator.dynamics;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import npcgd_simulator.graphs.Edge;
import npcgd_simulator.graphs.Vertex;
import npcgd_simulator.graphs.exceptions.NotAPortException;
import npcgd_simulator.graphs.normal.NormalEdgeStatus;
import npcgd_simulator.graphs.normal.NormalGraph;
import npcgd_simulator.graphs.normal.NormalVertexStatus;
import npcgd_simulator.graphs.pattern.PatternEdgeStatus;
import npcgd_simulator.graphs.pattern.PatternGraph;
import npcgd_simulator.graphs.pattern.PatternVertexStatus;
import npcgd_simulator.name_algebra.Name;
import npcgd_simulator.name_algebra.Renaming;
import npcgd_simulator.name_algebra.exceptions.NoRenamingException;
import npcgd_simulator.name_algebra.exceptions.UnauthorizedIntersectionException;


// Properties
// 1) Pattern and target have the same number of ports;
// 2) matching is an injection from pattern to target
// 3) matching is such that any pattern vertex v matches matching(v).
public class PatternMatching {

	private UnmutableLinkedPattern pattern;
	private Vertex<PatternVertexStatus, PatternEdgeStatus> start;
	private NormalGraph target;
	private Map<Vertex<PatternVertexStatus, PatternEdgeStatus>,
	    Vertex<NormalVertexStatus, NormalEdgeStatus>> matching;
	private Map<Integer, Integer> setValues;
	private Map<Integer, Integer> optionValues;
	private Renaming renaming;
	private Set<Vertex<PatternVertexStatus, PatternEdgeStatus>> targetDone;
		
  // Constructors	

  // Ensures properties by assertions
  protected PatternMatching(UnmutableLinkedPattern pattern, 
	  NormalGraph target, 
      Map<Vertex<PatternVertexStatus, PatternEdgeStatus>,
      Vertex<NormalVertexStatus, NormalEdgeStatus>> matching) 
      throws NotMatchingException {
    for (Vertex<PatternVertexStatus, PatternEdgeStatus> v 
           : pattern.getVertices()) {
      assert(matching.containsKey(v)); // Checks prop 2
      Vertex<NormalVertexStatus, NormalEdgeStatus> image = matching.get(v);
      assert(target.getVertices().contains(image)); // Check prop 2
      assert(v.getStatus().matches(matching.get(v).getStatus())); // Checks prop 3
    }
    assert (pattern.getNport() == target.getNport()); // Checks prop 1
    this.pattern = pattern;
    this.start = pattern.getVertices().iterator().next();
    this.target = target;
    this.matching = Collections.unmodifiableMap(
        new HashMap<Vertex<PatternVertexStatus, PatternEdgeStatus>,
            Vertex<NormalVertexStatus, NormalEdgeStatus>>(matching)
        );
    
    this.setValues = new HashMap<Integer, Integer>();
    this.optionValues = new HashMap<Integer, Integer>();
	this.renaming = new Renaming();

    this.targetDone = new HashSet<Vertex<PatternVertexStatus, PatternEdgeStatus>>();
    this.mkSetValues(start);
    this.targetDone.clear();
    this.mkOptionValues(start);
    this.targetDone.clear();
    this.mkRenaming();
    this.targetDone = null;
}
  
  protected static Set<PatternMatching> fromSet(UnmutableLinkedPattern pattern,
      NormalGraph target,
      Set<Map<Vertex<PatternVertexStatus, PatternEdgeStatus>,
          Vertex<NormalVertexStatus, NormalEdgeStatus>>> matchings)
      throws NotMatchingException{
    Set<PatternMatching> result = new HashSet<PatternMatching>(); 
    for (Map<Vertex<PatternVertexStatus, PatternEdgeStatus>,
        Vertex<NormalVertexStatus, NormalEdgeStatus>> matching : matchings)
      result.add(new PatternMatching(pattern, target, matching));
    return result;
  }

  // Getters
  
  protected PatternGraph getPattern() {
    return this.pattern;
  }
  
  protected NormalGraph getTarget() {
    return this.target;
  }
	
  protected Set<Map.Entry<
      Vertex<PatternVertexStatus, PatternEdgeStatus>,
      Vertex<NormalVertexStatus, NormalEdgeStatus>>>
      entrySet() {
    return this.matching.entrySet();
  }
  
  protected Vertex<NormalVertexStatus, NormalEdgeStatus>
      get(Vertex<PatternVertexStatus, PatternEdgeStatus> key) {
	assert (this.matching.containsKey(key));
	return this.matching.get(key);
  }
	
  protected Integer getSetValue(Integer setId) {
    assert(this.setValues.containsKey(setId));
    return this.setValues.get(setId);
  }
  
  protected boolean getIsOptional(Integer optionalId) {
    return this.optionValues.get(optionalId) == 1;
  }
  
  protected Name getRename(Name name) {
    try {
	  return name.rename(this.renaming);
	} catch (NoRenamingException e) {
	  // TO DO : PROVE
	  throw new Error("Cannot happen because of properties", e);
	}
  }
  
  // Auxiliary function for constructors
  
  private static <A, B> void checkOrAdd(Map<A, B> map, A key, B val)
      throws NotMatchingException {
	if (map.containsKey(key))
	  if(!map.get(key).equals(val))
	    throw new NotMatchingException();
	map.put(key, val);
  }
  
  private void mkSetValues(
	  Vertex<PatternVertexStatus, PatternEdgeStatus> start) 
      throws NotMatchingException {
    if (targetDone.contains(start))
	  return;
    targetDone.add(start);
    Vertex<NormalVertexStatus, NormalEdgeStatus> image = matching.get(start);
    
    PatternVertexStatus ps = start.getStatus();
	NormalVertexStatus ns = image.getStatus();
	if (ps.getSetId() != -1) {
	  Integer iSet = Integer.valueOf(ps.getSetId());
	  Integer iVal = Integer.valueOf(ns.getStatus());
	  checkOrAdd(this.setValues, iSet, iVal);
	}
	
	for (int i = 0; i < pattern.getNport(); i++) {
	  try {
	    if (!start.hasEdge(i) || !image.hasEdge(i))
	      continue;
	    Edge<PatternVertexStatus, PatternEdgeStatus> startEdge = 
	        start.getEdge(i);
	    if (startEdge.isBorder())
	      continue;
	    Edge<NormalVertexStatus, NormalEdgeStatus> imageEdge = 
	        image.getEdge(i);
	    Integer iSet = Integer.valueOf(startEdge.getStatus().getSetId());
		Integer iVal = Integer.valueOf(imageEdge.getStatus().getStatus());
        checkOrAdd(this.setValues, iSet, iVal);
	    mkSetValues(start.getNeighboor(i));
	  } catch (NotAPortException e) {
		  throw new Error("cannot happen");
	  }
	}
  }
  
  private void mkOptionValues(
	  Vertex<PatternVertexStatus, PatternEdgeStatus> start) 
      throws NotMatchingException {
    if (targetDone.contains(start))
	  return;
	targetDone.add(start);
	Vertex<NormalVertexStatus, NormalEdgeStatus> image = matching.get(start);
	
	for (int i = 0; i < pattern.getNport(); i++) {
	  try {
		if (!start.hasEdge(i) || !image.hasEdge(i))
		  continue;
	    Edge<PatternVertexStatus, PatternEdgeStatus> startEdge =
	        start.getEdge(i);
	    if (startEdge.isBorder())
	      continue;
	    Edge<NormalVertexStatus, NormalEdgeStatus> imageEdge = 
	        image.getEdge(i);
	    Integer iOpt = Integer.valueOf(startEdge.getStatus().getOptionalId());
	    Integer iVal = Integer.valueOf(imageEdge == null ? 0 : 1);
	    checkOrAdd(this.optionValues, iOpt, iVal);
	    mkSetValues(start.getNeighboor(i));
	  } catch (NotAPortException e) {
		  throw new Error("cannot happen");
	  }
	}
  }
  
  private void mkRenaming() {
    for (Map.Entry<Vertex<PatternVertexStatus, PatternEdgeStatus>, 
        Vertex<NormalVertexStatus, NormalEdgeStatus>> entry : 
        matching.entrySet())
		try {
	      this.renaming.infer(entry.getKey().getStatus().getName(), 
		      entry.getValue().getStatus().getName());
		} catch (UnauthorizedIntersectionException e) {
		  // TO DO : PROVE
		  throw new Error("Cannot happen because of properties"); 
		}
  }

}
