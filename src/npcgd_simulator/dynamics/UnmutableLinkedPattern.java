package npcgd_simulator.dynamics;

// Properties
// 0) Pattern has at least one vertex.
// 1) Pattern does not intersects itself.
// 3) Each border vertex has a counter image with the same status.
// 4) There is name preservation between patterns.
// 5) There is border ports preservation between patterns.
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import npcgd_simulator.MySetOps;
import npcgd_simulator.graphs.Edge;
import npcgd_simulator.graphs.Vertex;
import npcgd_simulator.graphs.exceptions.NotAPortException;
import npcgd_simulator.graphs.exceptions.UsedPortException;
import npcgd_simulator.graphs.normal.NormalEdgeStatus;
import npcgd_simulator.graphs.normal.NormalGraph;
import npcgd_simulator.graphs.normal.NormalVertexStatus;
import npcgd_simulator.graphs.pattern.PatternEdgeStatus;
import npcgd_simulator.graphs.pattern.PatternGraph;
import npcgd_simulator.graphs.pattern.PatternVertexStatus;

public class UnmutableLinkedPattern extends PatternGraph {
	
  private Map<Vertex<PatternVertexStatus, PatternEdgeStatus>, 
      Vertex<PatternVertexStatus, PatternEdgeStatus>> borderBinding;
  private UnmutableLinkedPattern counterImage;
  private UnmutableLinkedPattern image;
  
  private Set<Vertex<PatternVertexStatus, PatternEdgeStatus>> border;
  private Map<Vertex<PatternVertexStatus, PatternEdgeStatus>, Set<Integer>>
    borderPorts;

	  
  // Constructors
  
  public UnmutableLinkedPattern(PatternGraph pattern,
      UnmutableLinkedPattern counterImage,
      UnmutableLinkedPattern image) {
    super(pattern);
    assert(!pattern.isEmpty()); // Checks property 0
	this.borderBinding = 
        new HashMap<Vertex<PatternVertexStatus, PatternEdgeStatus>, 
            Vertex<PatternVertexStatus, PatternEdgeStatus>>();
	this.checkSelfIntersection();
	this.relink(counterImage);
	image.relink(this);
  }
  
  public UnmutableLinkedPattern(PatternGraph pattern) {
    super(pattern);
    assert(!pattern.isEmpty()); // Checks property 0
	this.borderBinding = 
        new HashMap<Vertex<PatternVertexStatus, PatternEdgeStatus>, 
            Vertex<PatternVertexStatus, PatternEdgeStatus>>();
	this.counterImage = this;
	this.image = this;
	this.relink(counterImage);
	image.relink(this);
	this.checkIntersection(this);
  }

  // Getters
  
  protected UnmutableLinkedPattern getCounterImage() {
    return this.counterImage;
  }
  
  protected UnmutableLinkedPattern getImage() {
    return this.image;
  }
	 
  private Vertex<PatternVertexStatus, PatternEdgeStatus> 
      getCounterImage(
      Vertex<PatternVertexStatus, PatternEdgeStatus> v) {
	assert(v.getStatus().isBorder());
	assert(this.borderBinding.containsKey(v));
    return this.borderBinding.get(v);
  }
  
  @Override
  public Set<Vertex<PatternVertexStatus, PatternEdgeStatus>> getBorder() {
    if (this.border == null)
      this.border = super.getBorder();
    return this.border;
  }
  
  protected Set<Integer> getBorderPorts(
      Vertex<PatternVertexStatus, PatternEdgeStatus> v) {
    assert(this.getVertices().contains(v));
	assert(v.getStatus().isBorder());
	assert(super.getBorder().contains(v));
	assert(this.getBorder().contains(v));
    if (this.borderPorts == null) {
      this.borderPorts =
          new HashMap<Vertex<PatternVertexStatus, PatternEdgeStatus>,
              Set<Integer>>();
      for (Vertex<PatternVertexStatus, PatternEdgeStatus> u :
          this.getBorder())
        this.borderPorts.put(u, u.getBorderPorts());
    }
    assert(this.borderPorts.get(v) != null);
    return this.borderPorts.get(v);
  }
  
  private Vertex<PatternVertexStatus, PatternEdgeStatus> getBorderBinding(
      Vertex<PatternVertexStatus, PatternEdgeStatus> borderVertex) {
    assert(this.getVertices().contains(borderVertex));
    assert(this.borderBinding.containsKey(borderVertex));
    return this.borderBinding.get(borderVertex);
  }

  // Making this graph unmutable
   
  @Override
  public Vertex<PatternVertexStatus, PatternEdgeStatus> 
     addVertex(PatternVertexStatus status) {
   throw new UnsupportedOperationException();
  }

  @Override
  public Edge<PatternVertexStatus, PatternEdgeStatus> 
     addEdge(Vertex<PatternVertexStatus, PatternEdgeStatus> vertex1,
     Vertex<PatternVertexStatus, PatternEdgeStatus> vertex2, int port1,
     int port2, PatternEdgeStatus status) {
   throw new UnsupportedOperationException(); 	  
  }

  @Override
  public boolean deleteVertex(
     Vertex<PatternVertexStatus, PatternEdgeStatus> v) {
   throw new UnsupportedOperationException();
  }
  
  // Functions to be used by Cycle class
  
  private void checkIntersectionMatching(
      Map<Vertex<PatternVertexStatus, PatternEdgeStatus>,
          Vertex<PatternVertexStatus, PatternEdgeStatus>> matching) {
    for (Map.Entry<Vertex<PatternVertexStatus, PatternEdgeStatus>,
        Vertex<PatternVertexStatus, PatternEdgeStatus>> entry : 
        matching.entrySet()) {
      assert(entry.getValue().getStatus().isBorder());
      assert(entry.getKey().getStatus().isBorder());
      Set<Integer> definedPorts1 = 
          MySetOps.not(this.getBorderPorts(entry.getKey()), this.getNport());
      Set<Integer> definedPorts2 = 
          MySetOps.not(this.getBorderPorts(entry.getValue()), this.getNport());
      assert(MySetOps.inter(definedPorts1, definedPorts2).isEmpty());
    }
  }
  
  protected void checkIntersection(UnmutableLinkedPattern pattern) {
    for (Map<Vertex<PatternVertexStatus, PatternEdgeStatus>,
        Vertex<PatternVertexStatus, PatternEdgeStatus>> matching :
        this.matches(pattern))
      checkIntersectionMatching(matching);
  }
   
  // Checking properties
  

    // Checking property 5
  private void checkBorderPorts() {
    for (Vertex<PatternVertexStatus, PatternEdgeStatus> v : this.getBorder()) {
      assert(this.getCounterImage(v) != null);
      assert(
          MySetOps.contains(this.getBorderPorts(v),
              counterImage.getBorderPorts(this.getCounterImage(v)))
      );
    }
  }

    // Checking property 1
  
  private void checkSelfIntersection() {
    this.checkIntersection(this);
  }
  
  private boolean link(Vertex<PatternVertexStatus, PatternEdgeStatus> v) {
    for (Vertex<PatternVertexStatus, PatternEdgeStatus> u : 
        counterImage.getBorder()) {
      if (u.getStatus().myEquals(v.getStatus())) {
        this.borderBinding.put(v, u);
        return true;
      }
    }
    return false;
  }
  
    // Checking property 3
  private void checkAndMakeBorderLinks() {
    for (Vertex<PatternVertexStatus, PatternEdgeStatus> v : this.getBorder())
      assert(link(v));
  }
  
    // Checking property 4
  private void checkNamePreservation() {
    assert(this.getLonelyBases().equals(this.counterImage.getLonelyBases()));
  }
  
  // Auxialiary functions

  protected void relink(UnmutableLinkedPattern counterImage) {
    this.counterImage = counterImage;
    this.checkAndMakeBorderLinks();
    this.checkBorderPorts();
    this.checkNamePreservation();
    this.counterImage.image = this;
  }

  // Application
 
  private Vertex<NormalVertexStatus, NormalEdgeStatus> instanciateVertex(
      NormalGraph target, PatternMatching patternMatching, 
      Vertex<PatternVertexStatus, PatternEdgeStatus> imagePatternVertex) {
    PatternVertexStatus imagePatternVertexStatus = 
        imagePatternVertex.getStatus();

    if (imagePatternVertexStatus.isBorder()) {
      return patternMatching.get(this.getBorderBinding(imagePatternVertex));
    }
    else {
      NormalVertexStatus instanceStatus;
      if (imagePatternVertexStatus.getSetId() == -1) {
        instanceStatus = 
            new NormalVertexStatus(
                patternMatching.getRename(imagePatternVertexStatus.getName()),
                imagePatternVertexStatus.getSet().getSingleton()
            );
      }
      else {
        instanceStatus = new NormalVertexStatus(
            patternMatching.getRename(imagePatternVertexStatus.getName()),
            patternMatching.getSetValue(imagePatternVertexStatus.getSetId())
            );
          
      }
      return target.addVertex(instanceStatus);
    }
  }
  
  private void instanciateEdge(
	  NormalGraph target, PatternMatching patternMatching,
      Vertex<PatternVertexStatus, PatternEdgeStatus> imagePatternVertex,
      int port,
      Map<Vertex<PatternVertexStatus, PatternEdgeStatus>,
      Vertex<NormalVertexStatus, NormalEdgeStatus>> done) {
    try {
      if (!imagePatternVertex.hasEdge(port))
        return;
      Edge<PatternVertexStatus, PatternEdgeStatus> imagePatternEdge = 
          imagePatternVertex.getEdge(port);
      if (imagePatternEdge.isBorder())
        return;
      if (imagePatternEdge.getStatus().isOptional() &&
          patternMatching.getIsOptional(
              imagePatternEdge.getStatus().getOptionalId())
         )
        return;
      if (!done.containsKey(imagePatternVertex.getNeighboor(port))) {
        recInstanciate(target, patternMatching,
            imagePatternVertex.getNeighboor(port), done);
        return;
      }
      Vertex<NormalVertexStatus, NormalEdgeStatus> neighboor = 
        done.get(imagePatternVertex.getNeighboor(port));
      PatternEdgeStatus imagePatternEdgeStatus = imagePatternEdge.getStatus();
      NormalEdgeStatus instanceEdgeStatus;
      if (imagePatternEdgeStatus.getSetId() == -1) {
        instanceEdgeStatus = new NormalEdgeStatus(
            imagePatternEdgeStatus.getSet().getSingleton());
      }
      else {
        instanceEdgeStatus = new NormalEdgeStatus(
            patternMatching.getSetValue((imagePatternEdgeStatus.getSetId())));
      }
      try {
        target.addEdge(done.get(imagePatternVertex), neighboor,
            imagePatternEdge.getPort(imagePatternVertex), 
            imagePatternEdge.getPort(imagePatternVertex.getNeighboor(port)),
            instanceEdgeStatus
        );
      } catch (UsedPortException exn) {
        // TO DO : PROVE
        throw new Error("cannot happen");
      }
    } catch (NotAPortException exn) {
      // TO DO : PROVE
      throw new Error("cannot happen");
    }
  }
  
  private Vertex<NormalVertexStatus, NormalEdgeStatus> recInstanciate(
	  NormalGraph target,
	  PatternMatching patternMatching,
      Vertex<PatternVertexStatus, PatternEdgeStatus> imagePatternVertex,
      Map<Vertex<PatternVertexStatus, PatternEdgeStatus>,
          Vertex<NormalVertexStatus, NormalEdgeStatus>> done) {
	  
	if (done.containsKey(imagePatternVertex))
      return done.get(imagePatternVertex);

    Vertex<NormalVertexStatus, NormalEdgeStatus> vertexInstance =
        instanciateVertex(target, patternMatching, imagePatternVertex);
    assert(vertexInstance != null);
	
	done.put(imagePatternVertex, vertexInstance);

	for (int port = 0; port < this.getNport(); port++)
	  instanciateEdge(target, patternMatching, imagePatternVertex, port, done);

	return vertexInstance;
  }
  
  // Requires
  // 1) patternMatching is defined on the 'counterImage' pattern.
  // This requirement is ensured by assertion below.
  protected void instanciate(PatternMatching patternMatching) {
	  
	NormalGraph target = patternMatching.getTarget();
	assert (patternMatching.getPattern() == this.counterImage);
	 
	// Delete old vertices.
    for (Map.Entry<Vertex<PatternVertexStatus, PatternEdgeStatus>, 
	    Vertex<NormalVertexStatus, NormalEdgeStatus>> entry : 
  	    patternMatching.entrySet())
      if (!entry.getKey().getStatus().isBorder()) {
        target.deleteVertex(entry.getValue());
      }
      else {
        for (Integer i : entry.getKey().getUsedPorts())
          try {
            if (entry.getValue().hasEdge(i))
              entry.getValue().getEdge(i).delete();
          } catch (NotAPortException ex) {
            assert (false);
          }
      }   
	  
	Map<Vertex<PatternVertexStatus, PatternEdgeStatus>,
        Vertex<NormalVertexStatus, NormalEdgeStatus>> done =
        new HashMap<Vertex<PatternVertexStatus, PatternEdgeStatus>,
            Vertex<NormalVertexStatus, NormalEdgeStatus>>();
	recInstanciate(target, patternMatching, this.getStart(), done);
	
  }
  
  protected Collection<PatternMatching> getMatches(NormalGraph target) {
	Collection<PatternMatching> matchings = new ArrayList<PatternMatching>();
    for (Map<Vertex<PatternVertexStatus, PatternEdgeStatus>,
        Vertex<NormalVertexStatus, NormalEdgeStatus>> matching : 
        this.counterImage.matches(target))
      try {
        matchings.add(new PatternMatching(this.counterImage, target, matching));
      } catch (NotMatchingException ex) {
      }
    return matchings;
  }
 
  protected void applyMatchings(Collection<PatternMatching> matchings) {
    for (PatternMatching matching : matchings)
      this.instanciate(matching);
  }
 
}