package npcgd_simulator;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;

import npcgd_simulator.dynamics.Dynamic;
import npcgd_simulator.graphs.normal.NormalGraph;
import npcgd_simulator.parser.ParseException;
import npcgd_simulator.parser.Parser;

public class Test {
 
  public static NormalGraph parseTestGraph(String file) 
      throws FileNotFoundException, ParseException {
    Reader r = new FileReader("/home/me/NPCGD_simulator/testfiles/" + file);
    Parser.ReInit(r);
    NormalGraph g = Parser.parseGraphFile();
    return g;
  }
  
  public static Dynamic parseTestDynamic(String file) 
      throws FileNotFoundException, ParseException {
    Reader r = new FileReader("/home/me/NPCGD_simulator/testfiles/" + file);
    Parser.ReInit(r);
    Dynamic d = Parser.parseDynamicFile();
    return d;
  }
  
  public static void main(String[] args)
      throws FileNotFoundException, ParseException {
	new Parser(System.in);
	NormalGraph g = parseTestGraph("graphMeyer");
	System.out.println(g);
    Dynamic d = parseTestDynamic("dynamicMeyer");
    System.out.println(d);
    d.apply(g);
    System.out.println(g);
    d.apply(g);
    System.out.println(g);
  }
}
