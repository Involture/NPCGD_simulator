JC=javac
JCFLAGS=-Xlint:all,-serial -d classes -cp classes -sourcepath src

all: 
	if $(JC) $(JCFLAGS) src/npcgd_simulator/Main.java;\
	then\
    echo done;\
  else\
	  make clean;\
	fi


test:
	if $(JC) $(JCFLAGS) src/npcgd_simulator/Test.java;\
	then\
    echo done;\
  else\
	  make clean;\
	fi

.PHONY:clean

clean:
	rm -r classes/*

